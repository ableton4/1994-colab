# 1994 Colab
Para colaborar hay que instalar los siguientes programas:

Estos comando se corren una sola vez.

- Verificar que git está instalado: `git --version`
- Si git NO está instalado: `brew install git`
- Instalar git lfs si NO está instalado ya: `git lfs install`

Descargar el proyecto:
- Abrir la terminal en la carpeta de proyectos de ableton y correr: `git clone https://gitlab.com/abletoon/1994-colab.git`
- `git lfs fetch --all`
- `git lfs pull`
- Abrir ableton y abrir el proyecto.
- Hacer click abajo de todo donde sale que faltan archivos.
- Hacer click en "Go" para que haga la busqueda automatica.
- Desplegar el menu de "Automatic Search" y en Search Folder seleccionen "Samples" y le dan a "Go" de nuevo.

Despues de eso no deberían saltarles más errores por falta de archivos.
